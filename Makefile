all: lib main
	make run

lib:
	gcc -shared -fPIC libUartRmc.c -o libUartRmc.o

main:
	gcc main.c libUartRmc.o -o main.out

ldlp:
	LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/mnt/g/projects/cfun/parser
	export LD_LIBRARY_PATH

run:
	./main.out