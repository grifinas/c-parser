#include <stdio.h>
#include <string.h>
#include "libUartRmc.h"

void Uart_OnByteReceived(uint8_t byte) 
{
	static rmc_t packet;
	static char buffer[64];
	static int bufindex = 0;
	static packet_state state = 0;

	if (byte == ',' || byte == '*' || (state == PKT_CHECKSUM && buffer[1] != 0)) {
		wordProcess(buffer, &packet, state);
		flushBuffer(buffer, &bufindex);
		state++;
	} else {
		buffer[bufindex] = byte;
		bufindex++;
	}
	//last state processed
	if (state == PKT_FINISHED) {
		printPacket(&packet);
	}
}

void wordProcess(char *buffer, rmc_t *packet, packet_state state)
{
	switch(state) {
		case PKT_TYPE:;
			char good_type[] = "$GPRMC";
			(*packet).status = RMC_DATA_VALID;
			for (int i=0; i<sizeof(good_type); i++) {
				if (buffer[i] != good_type[i]) {
					(*packet).status = RMC_DATA_INVALID;
					break;
				}
			}
			break;
		case PKT_TIMESTAMP:;
			float ts = strToNum(buffer);
			(*packet).time.hours = (int)ts/10000;
			(*packet).time.minutes = (int)ts/100%100;
			(*packet).time.seconds = (int)ts%100;
			(*packet).time.milliseconds = 0;
			break;
		case PKT_LATITUDE:;
			float lat = strToNum(buffer);
			(*packet).latitude = (int)(lat*1000000);
			break;
		case PKT_NS:;
			char ns = buffer[0];
			if (ns == 'N') {
				(*packet).ns = RMC_NORTH;
			} else if (ns == 'S') {
				(*packet).ns = RMC_SOUTH;
			} else {
				(*packet).status = RMC_DATA_INVALID;
			}
			break;
		case PKT_LONGITUDE:;
			float lng = strToNum(buffer);
			(*packet).longitude = (int)(lng*1000000);
			break;
		case PKT_EW:;
			char ew = buffer[0];
			if (ew == 'E') {
				(*packet).ew = RMC_EAST;
			} else if (ew == 'W') {
				(*packet).ew = RMC_WEST;
			} else {
				(*packet).status = RMC_DATA_INVALID;
			}
			break;
		case PKT_SPEED:;
			//Todo knots to km/h
			(*packet).speed = (short)strToNum(buffer);
			break;
		case PKT_COURSE:;
			(*packet).course = (short)strToNum(buffer);	
			break;
		case PKT_DATE:;
			float date = strToNum(buffer);
			(*packet).date.day = (int)date/10000;
			(*packet).date.month = (int)date/100%100;
			(*packet).date.year = (int)date%100;
			break;
	}
}

void flushBuffer(char *buffer, int *bufindex)
{
	memset(buffer, 0, sizeof(buffer));
	*bufindex = 0;
}

float strToNum(char *buffer)
{
	float num = 0;
	int dot = -1;
	int digit;
	int divisor;
	for (int i=0; i<sizeof(buffer); i++) {
		digit = buffer[i];
		if (digit == 46) {
			dot = i;
		}
		if (digit == 0) {
			break;
		}
		if (digit < 48 || digit > 57) {
			continue;
		}
		digit -= 48;
		if (dot == -1) {
			num = num*10 + digit;
		} else {
			divisor = 1;
			for (int j=0; j<(i-dot); j++) {
				divisor *= 10;
			}
			num = num + (float)digit / divisor;
		}
	}
	return num;
}

void printPacket(rmc_t *packet)
{
	rmc_t p = *packet;
	char ns = p.ns == RMC_NORTH ? 'N' : 'S';
	char ew = p.ew == RMC_EAST ? 'E' : 'W';
	printf("\n\nStatus: %i\nTime: %i-%i-%i %i:%i:%i\nLat: %i", p.status, p.date.year, p.date.month, p.date.day, p.time.hours, p.time.minutes, p.time.seconds, p.latitude);
	printf("%c, Lng: %i%c\nSpeed: %i\nCourse: %i\n", ns, p.longitude, ew, p.speed, p.course);
}