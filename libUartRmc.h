/**
 * @brief Data status indicator
 */
 
typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;


typedef enum {
    false,
    true
} bool;

typedef enum {
    PKT_TYPE,
    PKT_TIMESTAMP,
    PKT_VALIDITY,
    PKT_LATITUDE,
    PKT_NS,
    PKT_LONGITUDE,
    PKT_EW,
    PKT_SPEED,
    PKT_COURSE,
    PKT_DATE,
    PKT_VARIATION,
    PKT_VEW,
    PKT_CHECKSUM,
    PKT_FINISHED,
} packet_state;
 
typedef enum {
    RMC_DATA_INVALID,
    RMC_DATA_VALID
} rmc_status_t;

/**
 * @brief North/South indicator
 */
typedef enum {
    RMC_NORTH,
    RMC_SOUTH,
} rmc_ns_t;

/**
 * @brief East/West indicator
 */
typedef enum {
    RMC_EAST,
    RMC_WEST
} rmc_ew_t;

/**
 * @brief Time
 */
typedef struct {
    uint8_t hours;          //!< 0-23
    uint8_t minutes;        //!< 0-59
    uint8_t seconds;        //!< 0-59
    uint16_t milliseconds;  //!< 0-999
} rmc_time_t;

/**
 * @brief Date
 */
typedef struct {
    uint8_t day;    //!< 1-31
    uint8_t month;  //!< 1-12
    uint16_t year;  //!< 2000-2999
} rmc_date_t;

/**
 * @brief RMC packet data
 */
typedef struct {
    rmc_time_t time;
    rmc_status_t status;
    uint32_t latitude;       //!< Latitude in decimal degrees multiplied by 1 000 000
    rmc_ns_t ns;
    uint32_t longitude;      //!< Longitude in decimal degrees multiplied by 1 000 000
    rmc_ew_t ew;
    uint16_t speed;          //!< Speed in km/h
    uint16_t course;         //!< Course in degrees
    rmc_date_t date;
} rmc_t;

void Uart_OnByteReceived(uint8_t byte);
void flushBuffer(char *buffer, int *bufindex);
void wordProcess(char *buffer, rmc_t *packet, packet_state state);
float strToNum(char *buffer);
void printPacket(rmc_t *packet);